using System.Collections;
using System.Collections.Generic;
using System;


namespace Conversation
{
    namespace Parsing
    {   
        /// <summary>
        /// Class that represents an error during conversation parsing.
        /// </summary>
        [Serializable]
        public class ParserException : Exception
        {
            public ParserException(string message)
                : base(message) { }
        }
    }
}
