using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Conversation
{
    using Containers;
    namespace Parsing
    {
        using Tokenization;
        using Analysation;
        internal class Parser
        {
            /// <summary>
            /// Parses given text file. 
            ///
            /// Parameters:
            ///     - [Text to be parsed].
            ///     - [List of options to accept during parsing].
            /// 
            /// returns:
            ///     - [Parsing result as a Tuple].
            ///         - (Item1 parsing status).
            ///         - (Item2 is parsing result).
            /// </summary>
            internal Tuple<bool, ConversationContainer> Parse(string input, List<string> options) // TODO: return error for better localization.
            {
                Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
                SyntacticAnalysis analysis = new SyntacticAnalysis();
                try
                {
                    return analysis.Analyse(lexicalanalyser.GenerateTokenList(input), options);
                }
                catch(ParserException e) 
                {
                    Debug.LogException(e);
                    return new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
                }
            }
        }
    }
}


