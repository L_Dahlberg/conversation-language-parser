using System.Collections;
using System.Collections.Generic;
using System;


namespace Conversation
{
    namespace Parsing
    {
        namespace Tokenization
        {
            enum TOKEN
            {
                BRANCH_START,
                BRANCH_TARGET,
                OPTION,
                OPEN_BRACKET,
                CLOSE_BRACKET,
                STRING
            }

            /// <summary>
            /// Creates an instance of the given object, given their unique identifier.
            /// </summary>
            internal abstract class TokenGenerator
            {
                internal static Token Generate(char identifier)
                {
                    switch (identifier)
                    {
                        case '$':
                            return new Branch_start();
                        case '#':
                            return new Branch_target();
                        case '%':
                            return new Option();
                        case '\"':
                            return new String();
                        case '{':
                            return new OpenBracket(identifier);
                        case '}':
                            return new CloseBracket(identifier);
                        default:
                            throw new ParserException("Conversation:Parsing:Tokenization::TokenGenerator::Generate: Found unknown identifier '" + identifier + "' during token creation.");
                    }
                }

                internal static string TokenToString(TOKEN token)
                {
                    if (token == TOKEN.BRANCH_START)
                    {
                        return "branch_start";
                    }
                    if (token == TOKEN.BRANCH_TARGET)
                    {
                        return "branch_target";
                    }
                    if (token == TOKEN.OPTION)
                    {
                        return "option";
                    }
                    if (token == TOKEN.OPEN_BRACKET)
                    {
                        return "open_bracket";
                    }
                    if (token == TOKEN.CLOSE_BRACKET)
                    {
                        return "close_bracket";
                    }
                    if (token == TOKEN.STRING)
                    {
                        return "string";
                    }
                    return "";
                }
            }

            /// <summary>
            /// Class that represents any token type. Respective token type must inherit this.
            /// </summary>
            internal abstract class Token
            {
                internal TOKEN token;
                internal string value;

                internal abstract bool Add(char character);

                internal void ThrowException(char character)
                {
                    throw new ParserException("In Conversation:Parser:Tokenization::GenerateTokenList::Token: The token '" + token + "' with value '" + value + "' encountered invalid character '" + character + "'.");
                }

                internal void Clear()
                {
                    value = "";
                }
            }

            /// <summary>
            /// Token that represents the start of a branch. 
            /// Can only contain letters and underscore "_".
            /// </summary>
            internal class Branch_start : Token
            {

                internal Branch_start()
                {
                    token = TOKEN.BRANCH_START;
                }

                override internal bool Add(char character)
                {
                    if (Char.IsLetter(character) || Char.IsDigit(character) || character == '_')
                    {
                        value += character;
                        return true;
                    }
                    else if (Char.IsWhiteSpace(character))
                    {
                        return false;
                    }
                    ThrowException(character);
                    return false;
                }
            }

            /// <summary>
            /// Token that represents the end/target of a branch. 
            /// Can only contain letters and underscore "_".
            /// </summary>
            internal class Branch_target : Token
            {
                internal Branch_target()
                {
                    token = TOKEN.BRANCH_TARGET;
                }

                override internal bool Add(char character)
                {
                    if (Char.IsLetter(character) || Char.IsDigit(character) || character == '_')
                    {
                        value += character;
                        return true;
                    }
                    else if (Char.IsWhiteSpace(character))
                    {
                        return false;
                    }
                    ThrowException(character);
                    return false;
                }
            }

            /// <summary>
            /// Token that represents the any option. 
            /// Can only contain letters, numbers and underscore "_".
            /// </summary>
            internal class Option : Token
            {
                internal Option()
                {
                    token = TOKEN.OPTION;
                }

                override internal bool Add(char character)
                {
                    if (Char.IsLetter(character) || Char.IsDigit(character) || character == '_')
                    {
                        value += character;
                        return true;
                    }
                    else if (Char.IsWhiteSpace(character))
                    {
                        return false;
                    }
                    ThrowException(character);
                    return false;
                }
            }

            /// <summary>
            /// Token that represents the content that should be printed.
            /// Can contain anything except for the characters that represents the others, $, # and %.
            /// </summary>
            internal class String : Token
            {
                private bool complete = false;

                internal String()
                {
                    token = TOKEN.STRING;
                }

                override internal bool Add(char character)
                {
                    if(complete)
                    {
                        complete = false;
                        return false;
                    }
                    if(character != '\"' && character != '$' && character != '#' && character != '%' && character != '{' && character != '}')
                    {
                        value += character;
                        return true;
                    }
                    else if(character == '\"')
                    {
                        complete = true;
                        return true;
                    }
                    ThrowException(character);
                    return false;
                }

            }

            /// <summary>
            /// Token that represents the left Bracket.
            /// </summary>
            internal class OpenBracket : Token
            {
                internal OpenBracket(char identifier)
                {
                    value = identifier.ToString();
                    token = TOKEN.OPEN_BRACKET;
                }

                override internal bool Add(char character)
                {
                    return false;
                }

            }

            /// <summary>
            /// Token that represents the right Bracket.
            /// </summary>
            internal class CloseBracket : Token
            {
                internal CloseBracket(char identifier)
                {
                    value = identifier.ToString();
                    token = TOKEN.CLOSE_BRACKET;
                }

                override internal bool Add(char character)
                {
                    return false;
                }

            }
        }
    }
}
