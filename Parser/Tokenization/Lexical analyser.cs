using System;
using System.Collections;
using System.Collections.Generic;

namespace Conversation
{
    namespace Parsing
    {
        namespace Tokenization
        {
            internal class Lexicalanalyser
            {
                Token token;

                /// <summary>
                /// Creates a list of tokens based on the input string
                /// </summary>
                /// <param name="input"></param>
                /// <returns></returns>
                internal List<Token> GenerateTokenList(string input)
                {
                    List<Token> TokenList = new List<Token>();
                    token = null;
                    foreach(char character in input)
                    {
                        if(token == null)
                        {
                            GenerateToken(character);
                        }
                        else
                        {
                            if(!token.Add(character))
                            {
                                TokenList.Add(token);
                                GenerateToken(character);
                            }
                        }
                    }
                    if (token != null)
                    {
                        TokenList.Add(token);
                    }
                    return TokenList;
                }

                /// <summary>
                /// Generates a token given a character. Call this when previous token is complete.
                /// character cannot be a whiteSpace.
                /// </summary>
                /// <param name="character"></param>
                private void GenerateToken(char character)
                {
                    if (!Char.IsWhiteSpace(character))
                    {
                        token = TokenGenerator.Generate(character);
                    }
                    else
                    {
                        token = null;
                    }
                }
            }
        }
    }
}
