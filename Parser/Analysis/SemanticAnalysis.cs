using System.Collections;
using System.Collections.Generic;



namespace Conversation
{
    using Containers;
    namespace Parsing
    {
        using System;
        using Tokenization;
        namespace Analysation
        {
            /// <summary>
            /// Class used to determine the semantic correctness of the instructions. 
            /// 
            /// Specifically, it makes sure that each declared start branch token has a target branch token assoicated with it. 
            /// Additionally, it finds lone or multi-declared branch tokens of each type.
            /// Also verfies options.
            /// </summary>
            /// <param name="validOptions"></param>
            public class SemanticAnalysis
            {
                private List<string> validOptions;
                private Dictionary<string, BranchStart> ContatinerObjectReference = new Dictionary<string, BranchStart>();
                private Dictionary<string, bool> EncounteredBranches = new Dictionary<string, bool>();

                internal SemanticAnalysis(List<string> validOptions)
                {
                    this.validOptions = validOptions;
                }

                /// <summary>
                /// Checks whether all found branches are matched.
                /// </summary>
                /// <returns></returns>
                internal bool Correct()
                {
                    foreach (var entry in EncounteredBranches)
                    {
                        if(entry.Value == false)
                        {
                            throw new ParserException("In Conversation:Parser:Analysation::SemanticAnalysis::MatchBranch: The branch '" + entry.Key + "' with value '" + entry.Value + "' has no target branch.");
                        }
                    }
                    return true;
                }

                internal void MatchBranch(Token token, ref Dictionary<string, TOKEN> branches, ref BranchTarget branchTarget)
                {
                    if (branches.ContainsKey(token.value) && branches[token.value] == TOKEN.BRANCH_START
                        && EncounteredBranches.ContainsKey(token.value) && !EncounteredBranches[token.value])
                    {
                        ContatinerObjectReference[token.value].AddBranchTarget(branchTarget);
                        EncounteredBranches[token.value] = true;
                        branches.Remove(token.value);
                    }
                    else
                    {
                        throw new ParserException("In Conversation:Parser:Analysation::SemanticAnalysis::MatchBranch: The branch '" + token.token + "' with value '" + token.value + "' has not been previously declared.");
                    }
                }

                internal void MatchBranch(Token token, ref Dictionary<string, TOKEN> branches, ref BranchStart branchStart)
                {
                    if (!EncounteredBranches.ContainsKey(token.value))
                    {
                        EncounteredBranches[token.value] = false;
                        branches[token.value] = token.token;
                        ContatinerObjectReference[token.value] = branchStart;
                    }
                    else
                    {
                        throw new ParserException("In Conversation:Parser:Analysation::SemanticAnalysis::MatchBranch: The branch '" + token.token + "' with value '" + token.value + "' has already been declared.");
                    }
                }


                internal void Match(Token token)
                {
                    if (token.token == TOKEN.OPTION)
                    {
                        if (!validOptions.Contains(token.value))
                        {
                            throw new ParserException("In Conversation:Parser:Analysation::SemanticAnalysis::Match: The option '" + token.value + "' cannot be found in the list of valid options");
                        }
                    }
                }
            }
        }
    }
}
