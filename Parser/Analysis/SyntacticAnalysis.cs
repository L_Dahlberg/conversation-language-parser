using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace Conversation
{
    using Containers;
    namespace Parsing
    {
        using Tokenization;
        namespace Analysation
        {
            /// <summary>
            /// Class that parses the instructions given the following context free grammar:
            /// 
            /// {Stmt} -> {Expr} {Stmt} | (open_bracket){Begin branch}(close_bracket){Stmt} | empty
            /// {Begin branch} -> (branch start) {Expr} {List branch} {Target branch}
            /// {List branch} -> (branch start) {Expr} {List branch} {Target branch} | empty
            /// {Target branch} -> (branch target) {Stmt}
            /// {Expr} -> (string) | (option) (string)
            /// 
            /// </summary>
            internal class SyntacticAnalysis
            {
                ConversationContainer Conversation;
                List<Token> tokens;
                SemanticAnalysis SemanticAnalysis;

                /// <summary>
                /// Performs syntactic and semantic analysis on the given token sequences. Caller also provides a list of legal options.
                /// Provides the caller with a ConversatationContatainer containing the resulting instrucions as well as a bool indicating whether the process succeeded or not.
                /// </summary>
                /// <param name="tokens"></param>
                /// <param name="options"></param>
                /// <returns></returns>
                internal Tuple<bool, ConversationContainer> Analyse(List<Token> tokens, List<string> options)
                {
                    this.tokens = tokens;
                    SemanticAnalysis = new SemanticAnalysis(options);
                    Conversation = new ConversationContainer();

                    Conversation.Add();
                    if (Body(Conversation.GetBody()))
                    {
                        if(tokens.Count == 0)
                        {
                            if(SemanticAnalysis.Correct())
                            {
                                return new Tuple<bool, ConversationContainer>(true, Conversation);
                            }
                        }
                        throw new ParserException("In Conversation: Parser:Analysation::SyntacticAnalysis::Analyse: Conversation is not in accaptable format, '" + tokens.Count +  "' unused tokens found");
                    }
                    throw new ParserException("In Conversation: Parser:Analysation::SyntacticAnalysis::Analyse: Conversation is not in accaptable format");
                }

                private bool Body(Body body)
                {
                    body.Add();
                    var success = Expression(body.GetExpression()) && Body(body.GetBody()) || Match(TOKEN.OPEN_BRACKET) && BranchStart(body.GetBranchStart()) && Match(TOKEN.CLOSE_BRACKET) && Body(body.GetBody());
                    return true;
                }

                private bool BranchStart(BranchStart branchStart)
                {
                    branchStart.Add();
                    var branches = new Dictionary<string, TOKEN>();
                    var success = Match(TOKEN.BRANCH_START, ref branches, ref branchStart) && Expression(branchStart.GetExpression()) && ListBranch(ref branches, branchStart.GetBranchStart()) && BranchTarget(ref branches) && branches.Count == 0;
                    return success;
                }

                private bool ListBranch(ref Dictionary<string, TOKEN> branches, BranchStart branchStart)
                {
                    branchStart.Add();
                    var success = Match(TOKEN.BRANCH_START, ref branches, ref branchStart) && Expression(branchStart.GetExpression()) && ListBranch(ref branches, branchStart.GetBranchStart()) && BranchTarget(ref branches);
                    return true;
                }

                private bool BranchTarget(ref Dictionary<string, TOKEN> branches)
                {
                    BranchTarget branchTarget = new BranchTarget();
                    branchTarget.Add();
                    var success = Match(TOKEN.BRANCH_TARGET, ref branches, ref branchTarget) && Body(branchTarget.GetBody());
                    return success;
                }

                private bool Expression(Expression expression)
                {
                    var success = Match(TOKEN.STRING, expression) || Match(TOKEN.OPTION, expression) && Match(TOKEN.STRING, expression);
                    return success;
                }

                private bool Match(TOKEN desiredTokenType)
                {
                    if(tokens.Count != 0 && tokens[0].token == desiredTokenType)
                    {
                        tokens.RemoveAt(0);
                        return true;
                    }
                    return false;
                }

                private bool Match(TOKEN desiredTokenType, Expression expression)
                {
                    if (tokens.Count != 0 && tokens[0].token == desiredTokenType)
                    {
                        SemanticAnalysis.Match(tokens[0]);
                        expression.Add(tokens[0].token, tokens[0].value);
                        tokens.RemoveAt(0);
                        return true;
                    }
                    return false;
                }

                private bool Match(TOKEN desiredTokenType, ref Dictionary<string, TOKEN> branches, ref BranchTarget branchTarget)
                {
                    if (tokens.Count != 0 && tokens[0].token == desiredTokenType)
                    {
                        SemanticAnalysis.MatchBranch(tokens[0], ref branches, ref branchTarget);
                        tokens.RemoveAt(0);
                        return true;
                    }
                    return false;
                }

                private bool Match(TOKEN desiredTokenType, ref Dictionary<string, TOKEN> branches, ref BranchStart branchStart)
                {
                    if (tokens.Count != 0 && tokens[0].token == desiredTokenType)
                    {
                        SemanticAnalysis.MatchBranch(tokens[0], ref branches, ref branchStart);
                        tokens.RemoveAt(0);
                        return true;
                    }
                    return false;
                }
            }
        }
    }
}
