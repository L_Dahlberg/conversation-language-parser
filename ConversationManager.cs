using System;
using System.Collections.Generic;
using UnityEngine;

namespace Conversation
{
    using Containers;
    using Parsing;
    /// <summary>
    /// Class that executes the instructions encoded in the ConversationContainer
    /// </summary>
    internal class ConversationManager
    {
        internal bool Error { get; private set; }
        private string LookAheadValue;
        private bool isLooking = false;
        ConversationContainer conversation;

        private Action<string> ExecuteOption;
        private Action<string> PrintText;
        private Action<Dictionary<int, (string, string)>> PrintChoices;

        private Dictionary<int, BranchStart> BranchCurrentChoices = null;
        private Stack<Body> bodyStack = new Stack<Body>();


        /// <summary>
        /// Class that executes the instructions encoded in the string. 
        /// Parsing of the text happens at object creations. To start executing the instructitons call Start.
        /// 
        /// To get further instructions, perform continous calls to Continue.
        /// 
        /// </summary>
        /// <param name="ExecuteOption"></param>
        /// <param name="PrintText"></param>
        /// <param name="PrintChoices"></param>
        internal ConversationManager(string text, List<string> options)
        {
            var pair = new Parser().Parse(text, options);
            conversation = pair.Item2;
            Error = !pair.Item1;
        }

        /// <summary>
        /// Method that starts the instruction execution, starting with the first. Addional calls will restart exectution.
        ///
        /// In order to do this, it needs the delegate of three types of functions. These functions will be called to perform each given action at appropirate times.
        /// 
        /// [function ExecuteOption is called with the option value to be executed.]
        /// [funcion PrintText is called with a string of text to be printed.]
        /// [function PrintChoices is called with a dictionary containing the different choices to be printed.]
        /// </summary>
        /// <param name="ExecuteOption"></param>
        /// <param name="PrintText"></param>
        /// <param name="PrintChoices"></param>
        internal void Start(Action<string> ExecuteOption, Action<string> PrintText, Action<Dictionary<int, (string, string)>> PrintChoices)
        {
            this.ExecuteOption = ExecuteOption;
            this.PrintText = PrintText;
            this.PrintChoices = PrintChoices;

            BranchCurrentChoices = null;
            bodyStack.Clear();

            if (conversation.IsNotEmpty())
            {
                Body body = conversation.GetBody().IsNotEmpty() ? conversation.GetBody() : null;
                if (body != null)
                {
                    DisplayBody(body);
                    if(LookAhead() == "Branch")
                    {
                        Continue();
                    }
                }
            }
        }

        /// <summary>
        /// Method that continues by executing the next instruction, given the choice of the latest multichoice question.
        /// 
        /// returns false when the conversation is empty, otherwise true.
        /// </summary>
        /// <param name="choice"></param>
        /// <returns></returns>
        internal bool Continue(int choice = 0)
        {
            if (BranchCurrentChoices != null)
            {
                DisplayBody(BranchCurrentChoices[choice].GetBranchTarget().GetBody());
                BranchCurrentChoices = null;
            }
            else if (bodyStack.Count > 0)
            {
                DisplayBody(bodyStack.Pop());
            }
            else
            {
                return false;
            }
            if (LookAhead() == "Branch")
            {
                Continue();
            }
            return true;
        }

        /// <summary>
        /// Method that looks at the next instruction without executing it or removing it from the call stack.
        // The method takes the choice of the latest multichoice question.
        /// 
        /// returns a string representing the lookahead value. If called on the last line, returns "End".
        /// </summary>
        /// <param name="choice"></param>
        /// <returns></returns>
        internal string LookAhead(int choice = 0)
        {
            isLooking = true;
            LookAheadValue = "NotFound";
            if (BranchCurrentChoices != null)
            {
                DisplayBody(BranchCurrentChoices[choice].GetBranchTarget().GetBody());
                isLooking = false;
                return LookAheadValue;
            }
            else if (bodyStack.Count > 0)
            {
                DisplayBody(bodyStack.Peek());
                isLooking = false;
                return LookAheadValue;
            }
            isLooking = false;
            return "End";
        }

        private void DisplayBody(Body body)
        {
            Expression expression = body.GetExpression().IsNotEmpty() ? body.GetExpression() : null;
            BranchStart branchStart = body.GetBranchStart().IsNotEmpty() ? body.GetBranchStart() : null;
            if (expression != null)
            {
                if(isLooking)
                {
                    LookAheadValue = "Expression";
                }
                else
                {
                    DisplayExpression(expression);
                }
            }
            else if(branchStart != null && branchStart.GetBranchTarget() != null)
            {
                if (isLooking)
                {
                    LookAheadValue = "Branch";
                }
                else
                {
                    Branch(branchStart);
                }
            }
            if(CheckBody(body) && !isLooking)
            {
                bodyStack.Push(body.GetBody());
            }         
        }

        private bool CheckBody(Body body)
        {
            Body nextBody = body.GetBody().IsNotEmpty() ? body.GetBody() : null;
            if(nextBody == null)
            {
                return false;
            }

            BranchStart branchStart = nextBody.GetBranchStart().IsNotEmpty() ? nextBody.GetBranchStart() : null;
            Expression expression = nextBody.GetExpression().IsNotEmpty() ? nextBody.GetExpression() : null;
            Body anotherBody = nextBody.GetBody().IsNotEmpty() ? nextBody.GetBody() : null;

            if (branchStart == null && branchStart == null && anotherBody == null)
            {
                return false;
            }

            if (branchStart == null)
            {
                return true;
            }


            if (branchStart.GetBranchTarget() == null)
            {
                return false;
            }
            return true; 
        }

        private void Branch(BranchStart branchStart)
        {
            BranchCurrentChoices = new Dictionary<int, BranchStart>();

            int choiceIncrement = 0;
            BranchStart CurrentBranchStart = branchStart;

            while (CurrentBranchStart.GetBranchStart().IsNotEmpty())
            {
                BranchCurrentChoices[choiceIncrement++] = CurrentBranchStart;
                CurrentBranchStart = CurrentBranchStart.GetBranchStart();
            }

            Dictionary<int, (string, string)> ToPrint = new Dictionary<int, (string, string)>();
            foreach (var CurrentChoice in BranchCurrentChoices)
            {
                ToPrint[CurrentChoice.Key] = GetExpression(CurrentChoice.Value.GetExpression());
            }

            PrintChoices(ToPrint);
        }

        private void DisplayExpression(Expression expression)
        {
            string option = expression.GetOption();
            if (option != null)
            {
                ExecuteOption(option);
            }
            string text = expression.GetText();
            if (text != null)
            {
                PrintText(text);
            }
        }

        private (string, string) GetExpression(Expression expression)
        {
            string option = expression.GetOption();
            string text = expression.GetText();

            if (option != null && text != null)
            {
                return (option, text);
            }
            if (option == null && text != null)
            {
                return ("", text);
            }
            if (option != null && text == null)
            {
                return (option, "");
            }
            return ("", "");
        }
    }
}
