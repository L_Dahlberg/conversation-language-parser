# Conversation Language Parser

This C# library was written to be used as a conversation system tool for games in Unity. The library is a system that can take a customly written conversation and then give it back in chunks in a format that the user can display in their game.

The following is an example of a piece of code that can be written in the conversation language.

```
"Hi my name is blop"

%br "I live in the forest.."

%br "Do you want to talk?"
{
 $Yes "Yes"
 $No "No"

 #Yes 
 "Oh ok awesome! what is your favourite colour?"
 {
  $red %luck_20 "Red"
  $green "Green"
  $blue "Blue"

  #red "ugh"
  #green %influence_50 "yay me too!"
  #blue "ughh"
 }

 #No "oh.."
}

%br "Ok leave me alone"

```

When the library is given this text, it can be used to display text in a conversation window with multiple choices (branches) that lead to new snippets of text.

### Structure

The file must consist of the text to display enclosed around quotes (" ") intermixed with special tokens that tell the parser how to read everything. The following table show some usable tokens and what they do.

| Token | Description |
| ----------- | ----------- |
| " " |  Text to display|
| { } | Start and end of branch |
| $\<Name> | Start point of branch |
| #\<Name> | End point of branch |
| %\<Text> | Special option that can have any meaning. |

The file starts with the initial text that should be displayed. If branching is desired, the file should then place the two brackets '{}' containing one or more start and end points. After the branch is complete, the text following the '}' will be displayed.

Inside of the branch, the file must name one or more (unique) start points using the '$' token, followed by text to display alongside the choice. After declaring all starting points, the file must list the end points using the token '#' with the same unique identifier for each start point. 

The content that follows the '#' tokens and its attached name can be the same as the start of file. This means that nested branches can be placed at each branch end point. 

The '%' token can be used to inject any custom functionality that the game can make use of. For example, in our test case we defined the code "br" to mean "new line". This means that when the parser encounters the "%br" in the text, it will pass it as a special instruction to the game which can act accordingly.

The custom '%' token can also be used as breakpoints for the code to perform a special action. For example, '%increasePoints' and '%decreasePoints' can be strategely placed after specifc player choices to indicate to the code that points shall be increased or decreased. 

Note that for the current version, only one custom token can be placed along side a sentence. See chapter Language.

### How to use

Simply download and place the conversation-language-parser in your unity project. To start using it, include the namespace called 'Conversation' and create a ConversationManager. 

The *constructor* of the Conversation manager takes a string and a list of options. The string is the actual content, the code that is to be tokenized, analyzed and parsed by the library. The string can for example be hardcoded (like in the test cases here), written in to a string using the unity inspector or read from a file. The list of options must contain the names of all special options for the semantic analysis to pass.

After the creation of the Conversation manager, the user can check the field called 'Error' on the manager to see if the conversation was successfully parsed or not. If successful, the user can call the *Start* method on the Conversation manager to start getting fed lines to display in their game. Additonal calls to Start will restart the conversation.

The Start method takes three user defined functions as paramters. 

**The first paramter**:
  - A function for executing options. Must take one string as
    the only parameter. For example:

  ```
    void ExecuteOption(string option) {}
  ```

  - This function is called by the manager when it encounters an option in the
    text, the option itself arrives as a string.
  - The user can then do what ever they wish with it, depending on their 
    reason for putting it there in the first place (like increasing player points etc). 

**The second paramter**:
  - A function for printing text. Must take one string as a paramter. For example:

  ```
    void PrintText(string textToPrint) {}
  ```

  - This function is called when the manager encounters a sentence to print, the
    sentence arrives as the string.
  - The user can then display the given sentence in what ever way they wish.

**The third paramter**:
  - A function for printing the choices. Must take one Dictionary, where the 
    key is an integer and the value is a tuple with two string elements. For example:

    ```
    void PrintChoices(Dictionary<int, (string, string)> choices)
    ```
  - This function is called when the manager encounters a branching in the code. The keys of Dictionary represents the number for that choice (which must be used with the Continue method). The first item of the tuple represents the option that was placed before the sentence (if any). The second item of the tuple is the actual sentence to be displayed for that choice.
  - For example, when the manager sees the following lines:
  ```
    {
      $red %luck_20 "Red"
      $green "Green"
      $blue "Blue"

      #red "..."
      ...
    }
  ```

  It will call the PrintChoices function where the dictionary will look like:
  
```
    {0, Tuple(luck_20, Red)},
    {1, Tuple("", Green)},
    {2, Tuple("", Blue)}
```
  
After calling the Start method, the user will receive the first call to the appropriate function. In order to get another call, the user must call the *Continue* method to get the next one. In the case of branching, the continue method must be given the choice that the player made in order to know what path to take. The integer key of the *PrintChoices* dictonary represents the player choice, and is what should be given to the Continue method. If the user does not give any choice, the first one (0) will be chosen automatically.

Note that when Start or Continue is called, the manager will feed the user the next sentence (including the option). It then checks what is next using the *LookAhead* function (which can also be used externally). If LookAhead tells the manager that the next item is a branch (aka multiple choices), it will call Continue; making it be sent to the user. This is done in order for the player to be able to see the choices along side the question statement, without requiring the user to call LookAhead themselves after each Continue.

In order to run the test file *ConversationParsingTest*, simply add the script to any object in a unity scene. Run the game and check the console to see the test results. The test file can also be used further exemples of how to use the library, see the *ExecuteConversationTest* method.

### Language

The context free grammar (CFG) of the language:

```
G = {
      (Stmt, Begin branch, List branch, Target branch, Expr),
      (branch start, branch target, string, option, open bracket, close bracket),
      P,
      Stmt
    }

```

Defintion of rules P:

```
{Stmt} -> {Expr} {Stmt} | (open bracket){Begin branch}(close bracket){Stmt} | ε
{Begin branch} -> (branch start) {Expr} {List branch} {Target branch}
{List branch} -> (branch start) {Expr} {List branch} {Target branch} | ε
{Target branch} -> (branch target) {Stmt}
{Expr} -> (string) | (option) (string)
```

### Licence

Free to use. Please credit me if used in a released project.

Feel free to suggest changes via pull requests. 
