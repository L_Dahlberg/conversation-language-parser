using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conversation
{
    namespace Containers
    {
        using Parsing.Tokenization;

        /// <summary>
        /// Class that represents the expression, the string and/or the string, and the fifth and last rule of the CFG.
        /// </summary>
        internal class Expression : Container
        {
            internal override bool Empty { get; set; }
            internal string text = null;
            internal string option = null;
            
            internal Expression()
            {
                Empty = true;
            }

            internal override void Add() {}
            internal void Add(TOKEN tokenType, string value)
            {
                if(tokenType == TOKEN.STRING)
                {
                    text = value;
                }
                if(tokenType == TOKEN.OPTION)
                {
                    option = value;
                }
                Empty = false;
            }

            internal string GetText()
            {
                return text;
            }

            internal string GetOption()
            {
                return option;
            }
        }
    }
}
