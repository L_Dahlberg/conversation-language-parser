using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Conversation
{
    namespace Containers
    {
        /// <summary>
        /// Class that represents a basic container. All containers must inherit this.
        /// </summary>
        internal abstract class Container
        {
            internal abstract void Add();
            internal abstract bool Empty { get; set; }

            internal bool IsNotEmpty()
            {
                return !Empty;
            }
        }

        /// <summary>
        /// Class that represents the top logical container. If instructions are not empty, this will always haev a body references.
        /// </summary>
        internal class ConversationContainer : Container
        {
            internal override bool Empty { get; set; }
            internal Body body = null;

            internal ConversationContainer()
            {
                Empty = true;
            }

            internal override void Add()
            {
                if (Empty)
                {
                    body = new Body();
                }
                Empty = false;
            }

            internal Body GetBody()
            {
                return body;
            }
        }
    }
}
