using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conversation
{
    namespace Containers
    {
        /// <summary>
        /// Class that represents the end/target of a branch and the fourth rule of the CFG. 
        /// </summary>
        internal class BranchTarget : Container
        {
            internal override bool Empty { get; set; }
            internal Body body = null;

            internal BranchTarget()
            {
                Empty = true;
            }

            internal override void Add()
            {
                if (Empty)
                {
                    body = new Body();
                }
                Empty = false;
            }

            internal Body GetBody()
            {
                return body;
            }
        }
    }
}
