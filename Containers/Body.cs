using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conversation
{
    namespace Containers
    {
        /// <summary>
        /// Class that represents the first rule of the CFG. It encapsulates all variants of instructions.
        /// </summary>
        internal class Body : Container
        {
            internal override bool Empty { get; set; }
            internal Body body = null;
            internal Expression expression = null;
            internal BranchStart branchStart = null;

            internal Body()
            {
                Empty = true;
            }

            internal override void Add()
            {
                if (Empty)
                {
                    body = new Body();
                    expression = new Expression();
                    branchStart = new BranchStart();
                }
                Empty = false;
            }

            internal Body GetBody()
            {
                return body;
            }

            internal Expression GetExpression()
            {
                return expression;
            }

            internal BranchStart GetBranchStart()
            {
                return branchStart;
            }
        }
    }
}
