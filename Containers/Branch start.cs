using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conversation
{
    namespace Containers
    {
        /// <summary>
        /// Class that represents the start of a branch and the second and third rule of the CFG. 
        /// NOTE! The branch target reference is not the target_branch as seen in the CFG. Instead, it represents the actual destination of the branch.
        /// </summary>
        internal class BranchStart : Container
        {
            internal override bool Empty { get; set; }
            internal Expression expression = null;
            internal BranchTarget branchTarget = null;
            internal BranchStart branchStart = null;

            internal BranchStart()
            {
                Empty = true;
            }

            internal override void Add()
            {
                if (Empty)
                {
                    expression = new Expression();
                    branchStart = new BranchStart();
                }
                Empty = false;
            }

            internal void AddBranchTarget(BranchTarget branchTarget)
            {
                this.branchTarget = branchTarget;
            }

            internal Expression GetExpression()
            {
                return expression;
            }

            internal BranchTarget GetBranchTarget()
            {
                return branchTarget;
            }

            internal BranchStart GetBranchStart()
            {
                return branchStart;
            }
        }
    }
}
