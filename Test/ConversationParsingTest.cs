using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Conversation;
using Conversation.Containers;
using Conversation.Parsing;
using Conversation.Parsing.Tokenization;
using Conversation.Parsing.Analysation;
using System;


/// <summary>
/// Class that tests the conversation parser. 
/// </summary>
public class ConversationParsingTest : MonoBehaviour
{
    void Start()
    {
        TokenTest();
        ParserTest();
        ExecuteConversationTest();
        PrintStored();
    }

    private void ParserTest()
    {
        Parser parser = new Parser();
        Tuple<bool, ConversationContainer> conversation;
        string text;
        List<string> options;

        string SucessShortString()
        {
            text = "\"hey\" { $val \"whatsup\" $vall \"how u doin\" #val \"im gd\" #vall \"im guchi\" } %br \"aaa\"";
            options = new List<string> { "br", "luck_20", "influence_50" };
            conversation = parser.Parse(text, options);
            return GetStatusPrint(true, conversation.Item1);
        }

        string SucessLongString()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Y1es \"Yes\" $No \"No\" #Y1es \"Oh ok awesome! what is your favourite colour?\" { $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh..\" } %br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };
            conversation = parser.Parse(text, options);
            return GetStatusPrint(true, conversation.Item1);
        }

        string SucessExtraBodies()
        {
            text = "\"T_T\" \"-^-\" { $hola \"como\" #hola } \"estas\" \"<.<\" %br \"o7\" { $bro \"pls\" #bro } \"stooop\" \"pls\" { $omagod \"aaa\" $noo \"aaa\" #omagod \"aa\" { $wtf %bruh \"oo\" #wtf \"aa\" } #noo \"run\" }";
            options = new List<string> { "bruh", "br", "luck_20", "influence_50" };
            conversation = parser.Parse(text, options);
            return GetStatusPrint(true, conversation.Item1);
        }

        string FailUndeclaredBranch()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Yes \"Yes\" $No \"No\" #WTF \"Oh ok awesome! what is your favourite colour?\" { $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh.. } \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };
            
            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        string FailPreviouslyDeclaredBranch()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Yes \"Yes\" $No \"No\" #Yes \"Oh ok awesome! what is your favourite colour?\" { $Yes %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh.. } \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };

            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        string FailInvalidOption()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Yes \"Yes\" $No \"No\" #Yes \"Oh ok awesome! what is your favourite colour?\" { $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh.. } \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "WTF" };

            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        string FailMissingTargetBranch()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Yes \"Yes\" $No \"No\" #Yes \"Oh ok awesome! what is your favourite colour?\" $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #WTF \"oh.. } \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };

            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        string FailInvalidString()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to #talk ?\" { $Yes \"Yes\" $No \"No\" #Yes \"Oh ok awesome! what is your favourite colour?\" $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh.. } \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };

            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        string FailUnknownIdentifier()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Yes \"Yes\" $No \"No\" >Yes \"Oh ok awesome! what is your favourite colour?\" $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh.. } \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };

            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        string FailWrongBracket()
        {
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Yes \"Yes\" $No \"No\" >Yes \"Oh ok awesome! what is your favourite colour?\" $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh.. { \"%br \"Ok leave me alone\"";
            options = new List<string> { "br", "luck_20", "influence_50" };

            Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
            SyntacticAnalysis analysis = new SyntacticAnalysis();
            try
            {
                conversation = analysis.Analyse(lexicalanalyser.GenerateTokenList(text), options);
            }
            catch (ParserException)
            {
                conversation = new Tuple<bool, ConversationContainer>(false, new ConversationContainer());
            }
            return GetStatusPrint(false, conversation.Item1);
        }

        PreparePrint("Parsing", "", true);
        PreparePrint("SucessShortString", SucessShortString());
        PreparePrint("SucessLongString ", SucessLongString());
        PreparePrint("SucessExtraBodies ", SucessExtraBodies());
        PreparePrint("FailUndeclaredBranch ", FailUndeclaredBranch());
        PreparePrint("FailPreviouslyDeclaredBranch", FailPreviouslyDeclaredBranch());
        PreparePrint("FailInvalidOption\t ", FailInvalidOption());
        PreparePrint("FailMissingTargetBranch ", FailMissingTargetBranch());
        PreparePrint("FailInvalidString \t ", FailInvalidString());
        PreparePrint("FailUnknownIdentifier ", FailUnknownIdentifier());
        PreparePrint("FailWrongBracket ", FailWrongBracket());
    }

    private void TokenTest()
    {
        Lexicalanalyser lexicalanalyser = new Lexicalanalyser();
        var text = "\"a\" { $h2i \"bb\" #h2i \"oo\" %nerd \"ee\" } ";
        var expectedResult = new List<(string, string)>
        {
            ("string", "a"),
            ("open_bracket", "{"),
            ("branch_start", "h2i"),
            ("string", "bb"),
            ("branch_target", "h2i"),
            ("string", "oo"),
            ("option", "nerd"),
            ("string", "ee"),
            ("close_bracket", "}")
        };

        List<Token> tokens = lexicalanalyser.GenerateTokenList(text);


        string SuccessShortTokenList()
        {
            bool correct = true;
            int iterator = 0;
            string _token = "";
            string _value = "";
            foreach (Token token in tokens)
            {
                _token = expectedResult[iterator].Item1;
                _value = expectedResult[iterator].Item2;
                correct = correct && _value == token.value && _token == TokenGenerator.TokenToString(token.token);
                iterator++;
            }
            return GetStatusPrint(true, correct);
        }

        PreparePrint("Tokenization", "", true);
        PreparePrint("SuccessShortTokenList ", SuccessShortTokenList());
    }

    private void ExecuteConversationTest()
    {
        Parser parser = new Parser();
        ConversationManager executeConversation;
        string text;
        string result = "";
        string expectedResult;
        List<string> options;

        string SucessShortStringChoseFirstOnBranch()
        {
            void ExecuteOption(string option)
            {
                result += option != "" ? option + "|" : option;
            }
            void PrintText(string textToPrint)
            {
                result += textToPrint != "" ? textToPrint + "|" : textToPrint;
            }
            void PrintChoices(Dictionary<int, (string, string)> choices)
            {
                if (choices.ContainsKey(0))
                {
                    ExecuteOption(choices[0].Item1);
                    PrintText(choices[0].Item2);
                }
            }

            text = "\"hey\" { $val \"whatsup\" $vall \"how u doin\" #val \"im gd\" #vall } \"im guchi\"";
            expectedResult = "hey|whatsup|im gd|im guchi|";
            options = new List<string> { "br", "luck_20", "influence_50" };
            executeConversation = new ConversationManager(text, options);
            executeConversation.Start(ExecuteOption, PrintText, PrintChoices);
            bool Continue = true;
            while(Continue)
            {
                Continue = executeConversation.Continue(0);
            }
            return GetStatusPrint(true, result.CompareTo(expectedResult) == 0);
        }

        string SucessLongStringChoseFirstOnBranch()
        {
            void ExecuteOption(string option)
            {
                result += option != "" ? option + "|" : option;
            }
            void PrintText(string textToPrint)
            {
                result += textToPrint != "" ? textToPrint + "|" : textToPrint;
            }
            void PrintChoices(Dictionary<int, (string, string)> choices)
            {
                if(choices.ContainsKey(0))
                {
                    ExecuteOption(choices[0].Item1);
                    PrintText(choices[0].Item2);
                }
            }

            result = "";
            text = "\"Hi my name is blop\" %br \"I live in the forest..\" %br \"Do you want to talk ?\" { $Y1es \"Yes\" $No \"No\" #Y1es \"Oh ok awesome! what is your favourite colour?\" { $red %luck_20 \"Red\" $green \"Green\" $blue \"Blue\" #red \"ugh\" #green %influence_50 \"yay me too!\" #blue \"ugh\" } #No \"oh..\" } %br \"Ok leave me alone\"      ";
            expectedResult = "Hi my name is blop|br|I live in the forest..|br|Do you want to talk ?|Yes|Oh ok awesome! what is your favourite colour?|luck_20|Red|ugh|br|Ok leave me alone|";
            options = new List<string> { "br", "luck_20", "influence_50" };
            executeConversation = new ConversationManager(text, options);
            executeConversation.Start(ExecuteOption, PrintText, PrintChoices);
            bool Continue = true;
            while (Continue)
            {
                Continue = executeConversation.Continue(0);
            }
            return GetStatusPrint(true, result.CompareTo(expectedResult) == 0);
        }

        PreparePrint("Execue Conversation", "", true); 
        PreparePrint("SucessShortStringChoseFirstOnBranch  ", SucessShortStringChoseFirstOnBranch());
        PreparePrint("SucessLongStringChoseFirstOnBranch   ", SucessLongStringChoseFirstOnBranch());
    }


    //************ Printing *************


    private string GetStatusPrint(bool choice, bool status) => choice ? status ? "<color=green>PASSED</color>" : "<color=red>FAILED</color>" : status ? "<color=red>FAILED</color>" : "<color=green>PASSED</color>";


    private int Length = 0;
    private List<(string, string)> ToBePrinted = new List<(string, string)>();
    void PreparePrint(string TestName, string Result, bool start = false)
    {
        if(start)
        {
            ToBePrinted.Add(("____________________________\n\t         " + TestName + "Test:\n", ""));
        }
        else
        {
            string newLength = "\n        - " + TestName;
            Length = Length < newLength.Length ? newLength.Length : Length;  
            ToBePrinted.Add(("\n        - " + TestName, Result + "\n"));
        }
    }

    private void PrintStored()
    {
        Length += 100;
        var newLength = 0;
        foreach(var item in ToBePrinted)
        {
            newLength = Length - item.Item1.Length;
            Debug.Log(item.Item1.PadRight(newLength) + item.Item2);
        }
    }
}
